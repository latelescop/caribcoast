rem objectif : détection de la mangrove sur le site Kingston en Jamaique
rem bandes utilisées : B4 (rouge), B3 (vert), B2 (bleu), B8 (PIR) et B11 (SWIR) > sortir les 5 bandes du dossier Sentinel 2 téléchargé (chemin d'accès trop long)
rem en amont : 
	rem 1. création de l'emprise de la zone de l'étude, 
	rem 2. création des bases de de données apprentissage et validation,
	rem 3. traitements du DEM.
rem deux images couvrant la zone
rem date : 05/01/2020
rem projection : EPSG 32618

set PATH="C:\OTB-7.2.0-Win64\bin";%PATH%

rem Rééchantillonnage de la bande 11 à 20m avec la bande 4 à 10m > bande 11 à 10m sur l'image T18QUE
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B04_10m.jp2"
set input_B11_20="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B11_20m.jp2"
set output_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B11_10m.tif"
call otbcli_Superimpose -inr %input_B4_10% -inm %input_B11_20% -out %output_B11_10% uint16 

rem Rééchantillonnage de la bande 11 à 20m avec la bande 4 à 10m > bande 11 à 10m sur l'image T18QTE
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B04_10m.jp2"
set input_B11_20="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B11_20m.jp2"
set output_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B11_10m.tif"
call otbcli_Superimpose -inr %input_B4_10% -inm %input_B11_20% -out %output_B11_10% uint16

rem Fusion des bandes B4, B3, B2, B8 et B11, toutes à 10m sur l'image T18QUE
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B04_10m.jp2"
set input_B3_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B03_10m.jp2"
set input_B2_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B02_10m.jp2"
set input_B8_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B08_10m.jp2"
set input_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QUE_20200105T154541_B11_10m.tif"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\S2A_MSIL2A_20200105_T18QUE_B4_B3_B2_B8_B11.tif"
call otbcli_ConcatenateImages -il %input_B4_10% %input_B3_10% %input_B2_10% %input_B8_10% %input_B11_10% -out %output_5B% uint16

rem Fusion des bandes B4, B3, B2, B8 et B11, toutes à 10m sur l'image T18QTE
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B04_10m.jp2"
set input_B3_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B03_10m.jp2"
set input_B2_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B02_10m.jp2"
set input_B8_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B08_10m.jp2"
set input_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\T18QTE_20200105T154541_B11_10m.tif"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\S2A_MSIL2A_20200105_T18QTE_B4_B3_B2_B8_B11.tif"
call otbcli_ConcatenateImages -il %input_B4_10% %input_B3_10% %input_B2_10% %input_B8_10% %input_B11_10% -out %output_5B% uint16

rem Mosaïque des deux images T18QUE et T18QTE
set input_5B_img1="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\S2A_MSIL2A_20200105_T18QUE_B4_B3_B2_B8_B11.tif"
set input_5B_img2="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\S2A_MSIL2A_20200105_T18QTE_B4_B3_B2_B8_B11.tif"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11.tif"
call otbcli_Mosaic -il %input_5B_img1% %input_5B_img2% -harmo.method band -interpolator bco -out %output_5B% uint16

rem Découpage de l'image selon l'emprise de la zone de l'étude
set input_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11.tif"
set input_emprise="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\vecteur\zone_Kingston.shp"
set output_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif"
call otbcli_extractroi -in %input_5B% -mode fit -mode.fit.vect %input_emprise% -out %output_5B_clip% uint16

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\

rem CLASSIFICATION RANDOM FOREST

rem Calcul des statistiques de l'image raster Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif"
set output_stat_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\stat_S2A_MSIL2A_Kingston5B.xml"
call otbcli_ComputeImagesStatistics -il %input_5B_clip% -out %output_stat_5B_clip%

rem Entraînement du modèle statistique avec les données d'apprentissage
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif"
set input_BD_app="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\vecteur\BD\BD_Kingston_Apprentissage.shp"
set input_stat_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\stat_S2A_MSIL2A_Kingston5B.xml"
set output_model_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\RF_S2A_MSIL2A_Kingston5B.model"
set output_confmatout_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\RF_S2A_MSIL2A_Kingston5B.csv"
call otbcli_TrainImagesClassifier -io.il %input_5B_clip% -io.vd %input_BD_app% -io.imstat %input_stat_5B_clip% -sample.vtr 0.5 -sample.vfn id -classifier rf -classifier.rf.min 20 -classifier.rf.nbtrees 200 -io.out %output_model_5B_clip% -io.confmatout %output_confmatout_5B_clip%

rem Classification de l'image avec le modèle statistique
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif"
set input_model_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\RF_S2A_MSIL2A_Kingston5B.model"
set input_stat_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\stat_S2A_MSIL2A_Kingston5B.xml"
set output_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\Classif_RF_Kingston5B.tif"
call otbcli_ImageClassifier -in %input_5B_clip% -model %input_model_5B_clip% -imstat %input_stat_5B_clip% -out %output_RF_5B_clip% uint8

rem Evaluation statistique de la classification avec la matrice de confusion
set input_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\Classif_RF_Kingston5B.tif"
set output_matix_conf_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\Matrix_Conf_classif_RF_Kingston5B.csv"
set input_BD_val="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\vecteur\BD\BD_Kingston_Validation.shp"
call otbcli_ComputeConfusionMatrix -in %input_RF_5B_clip% -out %output_matix_conf_RF_5B_clip% -ref vector -ref.vector.in %input_BD_val% -ref.vector.field id
rem copie d'écran à faire pour avoir les informations de la précision, du rappel et du f-score de chaque classe

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\binarisation-filtrage\

rem : deux COPDEM sur Kingston
rem Reprojection des COPDEM : gdal_warp sous QGIS > Copernicus_DSM_10_N17_00_W077_00_DEM_reproj32618.tif et Copernicus_DSM_10_N18_00_W077_00_DEM_reproj32618.tif
rem Fusion des COPDEM : gdal_merge sous QGIS > Copernicus_2011_DEM_reproj32618.tif

rem Découpage du DEM avec l'emprise de l'image Sentinel 2 découpée Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif
set input_COPDEM="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\Kingston\Copernicus_2011_DEM_reproj32618.tif"
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif"
set output_COPDEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\Kingston\Copernicus_2011_DEM_reproj32618_Kingston.tif"
call otbcli_extractroi -in %input_COPDEM% -mode fit -mode.fit.im %input_5B_clip% -out %output_COPDEM_clip% float

rem Rééchantillonnage du DEM par rapport à la résolution de l'image Sentinel 2 découpée Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\raster\Mosaic_S2A_MSIL2A_T18_20200105_B4_B3_B2_B8_B11_Kingston.tif"
set input_COPDEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\Kingston\Copernicus_2011_DEM_reproj32618_Kingston.tif"
set output_COPDEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\Kingston\Copernicus_2011_DEM_reproj32618_Kingston_10m.tif"
call otbcli_Superimpose -inr %input_5B_clip% -inm %input_COPDEM_clip% -out %output_COPDEM_10%

rem Binarisation mangroves = 1 et autres classes = 0 + filtrage avec l'altitude en prenant un DEM (==3 signifie la 3ème classe de la classification = mangroves)
rem Changer dans l'expression de BandMathX l'altitude
set input_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\classification\Classif_RF_Kingston5B.tif"
set input_COPDEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\Kingston\Copernicus_2011_DEM_reproj32618_Kingston_10m.tif"
set output_mangroves_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\binarisation-filtrage\Classif_RF_Kingston_Mangroves_10m.tif"
call otbcli_BandMathX -il %input_RF_5B_clip% %input_COPDEM_10% -out %output_mangroves_clip% uint8 -exp "im2b1<=10&&im1b1==3?1:0"

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\Kingston\lissage\

rem manip à faire manuellement sur QGIS : QGIS > Raster > Analyse > Tamiser (seuil = 5 ; Cocher "Ne pas utiliser le masque de validité par défaut pour les bandes en entrée") > nom de sortie : Classif_RF_Kingston_Mangroves_10m_Tam5