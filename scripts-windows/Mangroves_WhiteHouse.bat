rem objectif : détection de la mangrove sur le site WhiteHouse en Jamaique
rem bandes utilisées : B4 (rouge), B3 (vert), B2 (bleu), B8 (PIR) et B11 (SWIR) > sortir les 5 bandes du dossier Sentinel 2 téléchargé (chemin d'accès trop long)
rem en amont : 
	rem 1. création de l'emprise de la zone de l'étude, 
	rem 2. création des bases de de données apprentissage et validation,
	rem 3. traitements du DEM.
rem une seule image couvrant la zone
rem date : 23/03/2020
rem projection : EPSG 32617

set PATH="C:\OTB-7.2.0-Win64\bin";%PATH%

rem Rééchantillonnage de la bande 11 à 20m avec la bande 4 à 10m > bande 11 à 10m
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B04_10m.jp2"
set input_B11_20="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B11_20m.jp2"
set output_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B11_10m.jp2"
call otbcli_Superimpose -inr %input_B4_10% -inm %input_B11_20% -out %output_B11_10% uint16

rem Fusion des bandes B4, B3, B2, B8 et B11, toutes à 10m
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B04_10m.jp2"
set input_B3_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B03_10m.jp2"
set input_B2_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B02_10m.jp2"
set input_B8_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B08_10m.jp2"
set input_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\T17QRA_20200323T155529_B11_10m.jp2"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20191229_B4_B3_B2_B8_B11.tif"
call otbcli_ConcatenateImages -il %input_B4_10% %input_B3_10% %input_B2_10% %input_B8_10% %input_B11_10% -out %output_5B% uint16

rem Découpage de l'image selon l'emprise de la zone de l'étude
set input_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11.tif"
set input_emprise="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\vecteur\Zone_Whitehouse.shp"
set output_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif"
call otbcli_extractroi -in %input_5B% -mode fit -mode.fit.vect %input_emprise% -out %output_5B_clip% uint16

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\

rem CLASSIFICATION RANDOM FOREST

rem Calcul des statistiques de l'image raster S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif"
set output_stat_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\stat_S2B_MSIL2A_WhiteHouse5B.xml"
call otbcli_ComputeImagesStatistics -il %input_5B_clip% -out %output_stat_5B_clip%

rem Entraînement du modèle statistique avec les données d'apprentissage
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif"
set input_BD_app="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\vecteur\BD\BD_Whitehouse_Apprentissage.shp"
set input_stat_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\stat_S2B_MSIL2A_WhiteHouse5B.xml"
set output_model_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\RF_S2B_MSIL2A_WhiteHouse5B.model"
set output_confmatout_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\RF_S2B_MSIL2A_WhiteHouse5B.csv"
call otbcli_TrainImagesClassifier -io.il %input_5B_clip% -io.vd %input_BD_app% -io.imstat %input_stat_5B_clip% -sample.vtr 0.5 -sample.vfn id -classifier rf -classifier.rf.min 20 -classifier.rf.nbtrees 200 -io.out %output_model_5B_clip% -io.confmatout %output_confmatout_5B_clip%

rem Classification de l'image avec le modèle statistique
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif"
set input_model_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\RF_S2B_MSIL2A_WhiteHouse5B.model"
set input_stat_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\stat_S2B_MSIL2A_WhiteHouse5B.xml"
set output_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\Classif_RF_WhiteHouse5B.tif"
call otbcli_ImageClassifier -in %input_5B_clip% -model %input_model_5B_clip% -imstat %input_stat_5B_clip% -out %output_RF_5B_clip% uint8

rem Evaluation statistique de la classification avec la matrice de confusion
set input_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\Classif_RF_WhiteHouse5B.tif"
set output_matix_conf_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\Matrix_Conf_classif_RF_WhiteHouse5B.csv"
set input_BD_val="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\vecteur\BD\BD_Whitehouse_Validation.shp"
call otbcli_ComputeConfusionMatrix -in %input_RF_5B_clip% -out %output_matix_conf_RF_5B_clip% -ref vector -ref.vector.in %input_BD_val% -ref.vector.field id
rem copie d'écran à faire pour avoir les informations de la précision, du rappel et du f-score de chaque classe

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\binarisation-filtrage\

rem : deux COPDEM sur WhiteHouse
rem Reprojection des COPDEM : gdal_warp sous QGIS > Copernicus_DSM_10_N18_00_W078_00_DEM_reproj32617.tif et Copernicus_DSM_10_N18_00_W079_00_DEM_reproj32617.tif
rem Fusion des COPDEM : gdal_merge sous QGIS > Copernicus_2011_DEM_reproj32617.tif

rem Découpage du DEM avec l'emprise de l'image Sentinel 2 découpée S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif
set input_COPDEM="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\WhiteHouse\Copernicus_2011_DEM_reproj32617.tif"
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif"
set output_COPDEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\WhiteHouse\Copernicus_2011_DEM_reproj32617_WhiteHouse.tif"
call otbcli_extractroi -in %input_COPDEM% -mode fit -mode.fit.im %input_5B_clip% -out %output_COPDEM_clip% float

rem Rééchantillonnage du DEM par rapport à la résolution de l'image Sentinel 2 découpée S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\raster\S2B_MSIL2A_20200323_B4_B3_B2_B8_B11_WhiteHouse.tif"
set input_COPDEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\WhiteHouse\Copernicus_2011_DEM_reproj32617_WhiteHouse.tif"
set output_COPDEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\WhiteHouse\Copernicus_2011_DEM_reproj32617_WhiteHouse_10m.tif"
call otbcli_Superimpose -inr %input_5B_clip% -inm %input_COPDEM_clip% -out %output_COPDEM_10%

rem Binarisation mangroves = 1 et autres classes = 0 + filtrage avec l'altitude en prenant un DEM (==3 signifie la 3ème classe de la classification = mangroves)
rem Changer dans l'expression de BandMathX l'altitude
set input_RF_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\classification\Classif_RF_WhiteHouse5B.tif"
set input_COPDEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\DEM_Jamaique\COPDEM\WhiteHouse\Copernicus_2011_DEM_reproj32617_WhiteHouse_10m.tif"
set output_mangroves_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\binarisation-filtrage\Classif_RF_WhiteHouse_Mangroves_15m.tif"
call otbcli_BandMathX -il %input_RF_5B_clip% %input_COPDEM_10% -out %output_mangroves_clip% uint8 -exp "im2b1<=15&&im1b1==3?1:0"


call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Jamaique\WhiteHouse\lissage\

rem manip à faire manuellement sur QGIS : QGIS > Raster > Analyse > Tamiser (seuil = 5 ; Cocher "Ne pas utiliser le masque de validité par défaut pour les bandes en entrée") > nom de sortie : Classif_RF_WhiteHouse_Mangroves_15m_Tam5