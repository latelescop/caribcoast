rem objectif : détection de la mangrove sur le site de Rincon à Porto Rico
rem bandes utilisées : B4 (rouge), B3 (vert), B2 (bleu), B8 (PIR) et B11 (SWIR) > sortir les 5 bandes du dossier Sentinel 2 téléchargé (chemin d'accès trop long)
rem en amont : 
	rem 1. création de l'emprise de la zone de l'étude, 
	rem 2. création d'un masque de nuages + ombres,
	rem 3. création des bases de de données apprentissage et validation,
	rem 4. traitements du DEM.
rem deux images couvrant la zone
rem date : 17/02/2020 et 27/02/2020
rem projection : EPSG 32619

set PATH="C:\OTB-7.2.0-Win64\bin";%PATH%

rem Rééchantillonnage de la bande 11 à 20m avec la bande 4 à 10m > bande 11 à 10m sur l'image T19QFA_20200217
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B04_10m.jp2"
set input_B11_20="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B11_20m.jp2"
set output_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B11_10m.tif"
call otbcli_Superimpose -inr %input_B4_10% -inm %input_B11_20% -out %output_B11_10% uint16 

rem Rééchantillonnage de la bande 11 à 20m avec la bande 4 à 10m > bande 11 à 10m sur l'image T19QFA_20200227
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B04_10m.jp2"
set input_B11_20="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B11_20m.jp2"
set output_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B11_10m.tif"
call otbcli_Superimpose -inr %input_B4_10% -inm %input_B11_20% -out %output_B11_10% uint16

rem Fusion des bandes B4, B3, B2, B8 et B11, toutes à 10m sur l'image T19QFA_20200217
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B04_10m.jp2"
set input_B3_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B03_10m.jp2"
set input_B2_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B02_10m.jp2"
set input_B8_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B08_10m.jp2"
set input_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200217T150719_B11_10m.tif"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\S2B_MSIL2A_20200217_B4_B3_B2_B8_B11.tif"
call otbcli_ConcatenateImages -il %input_B4_10% %input_B3_10% %input_B2_10% %input_B8_10% %input_B11_10% -out %output_5B% uint16

rem Fusion des bandes B4, B3, B2, B8 et B11, toutes à 10m sur l'image T19QFA_20200227
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B04_10m.jp2"
set input_B3_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B03_10m.jp2"
set input_B2_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B02_10m.jp2"
set input_B8_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B08_10m.jp2"
set input_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\T19QFA_20200227T150719_B11_10m.tif"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\S2B_MSIL2A_20200227_B4_B3_B2_B8_B11.tif"
call otbcli_ConcatenateImages -il %input_B4_10% %input_B3_10% %input_B2_10% %input_B8_10% %input_B11_10% -out %output_5B% uint16

rem Mosaïque des deux images T19QFA_20200217 et T19QFA_20200227
set input_5B_img1="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\S2B_MSIL2A_20200217_B4_B3_B2_B8_B11.tif"
set input_5B_img2="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\S2B_MSIL2A_20200227_B4_B3_B2_B8_B11.tif"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\S2B_MSIL2A_202002_B4_B3_B2_B8_B11.tif"
call otbcli_Mosaic -il %input_5B_img1% %input_5B_img2% -harmo.method band -interpolator bco -out %output_5B% uint16

rem Découpage de l'image selon l'emprise de la zone de l'étude
set input_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\S2B_MSIL2A_202002_B4_B3_B2_B8_B11.tif"
set input_emprise="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\vecteur\zone_Rincon.shp"
set output_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif"
call otbcli_extractroi -in %input_5B% -mode fit -mode.fit.vect %input_emprise% -out %output_5B_clip% uint16

rem MASQUE NUAGES + OMBRES

rem Rasterisation du masque
set input_mask_shp="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\masque\mask_nuages_S2_202002.shp"
set input_ref_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif"
set output_mask_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\masque\mask_nuages_S2_202002.tif"
call otbcli_rasterization -in %input_mask_shp% -im %input_ref_5B_clip% -szx 10 -szy 10 -epsg 32619 -background 0 -mode attribute -mode.attribute.field id -out %output_mask_raster%

rem Application du masque de nuages + ombres sur Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif"
set input_mask_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\masque\mask_nuages_S2_202002.tif"
set output_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon_nocloud.tif"
call otbcli_BandMathX -il %input_5B_clip% %input_mask_raster% -out %output_5B_clip_nocloud%?nodata=65535 uint16 -exp "im2b1==1?{65535,65535,65535,65535,65535}:im1"

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\

rem CLASSIFICATION RANDOM FOREST

rem Inversion du masque nuages + ombres
set input_mask_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\masque\mask_nuages_S2_202002.tif"
set output_mask_inv_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\masque\mask_nuages_S2_202002_inv.tif"
call otbcli_BandMathX -il %input_mask_raster% -out %output_mask_inv_raster% uint8 -exp "im1b1==1?0:1"

rem Calcul des statistiques de l'image raster S2A_MSIL2A_20200222_B4_B3_B2_B8_B11_Fajardo_nocloud.tif
set input_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon_nocloud.tif"
set output_stat_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\stat_S2B_MSIL2A_Rincon5B_nocloud.xml"
call otbcli_ComputeImagesStatistics -il %input_5B_clip_nocloud% -out %output_stat_5B_clip_nocloud% -bv 65535

rem Entraînement du modèle statistique avec les données d'apprentissage
set input_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon_nocloud.tif"
set input_BD_app="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\vecteur\BD\BD_Rincon_Apprentissage.shp"
set input_stat_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\stat_S2B_MSIL2A_Rincon5B_nocloud.xml"
set output_model_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\RF_S2B_MSIL2A_Rincon5B_nocloud.model"
set output_confmatout_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\RF_S2B_MSIL2A_Rincon5B_nocloud.csv"
call otbcli_TrainImagesClassifier -io.il %input_5B_clip_nocloud% -io.vd %input_BD_app% -io.imstat %input_stat_5B_clip_nocloud% -sample.vtr 0.5 -sample.vfn id -classifier rf -classifier.rf.min 20 -classifier.rf.nbtrees 200 -io.out %output_model_5B_clip_nocloud% -io.confmatout %output_confmatout_5B_clip_nocloud%

rem Classification de l'image avec le modèle statistique
set input_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon_nocloud.tif"
set input_mask_inv_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\masque\mask_nuages_S2_202002_inv.tif"
set input_model_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\RF_S2B_MSIL2A_Rincon5B_nocloud.model"
set input_stat_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\stat_S2B_MSIL2A_Rincon5B_nocloud.xml"
set output_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\Classif_RF_Rincon5B_nocloud.tif"
call otbcli_ImageClassifier -in %input_5B_clip_nocloud% -mask %input_mask_inv_raster% -model %input_model_5B_clip_nocloud% -imstat %input_stat_5B_clip_nocloud% -out %output_RF_5B_clip_nocloud% uint8

rem Evaluation statistique de la classification avec la matrice de confusion
set input_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\Classif_RF_Rincon5B_nocloud.tif"
set output_matix_conf_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\Matrix_Conf_classif_RF_Rincon5B_nocloud.csv"
set input_BD_val="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\vecteur\BD\BD_Rincon_Validation.shp"
call otbcli_ComputeConfusionMatrix -in %input_RF_5B_clip_nocloud% -out %output_matix_conf_RF_5B_clip_nocloud% -ref vector -ref.vector.in %input_BD_val% -ref.vector.field id
rem copie d'écran à faire pour avoir les informations de la précision, du rappel et du f-score de chaque classe

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\binarisation-filtrage\

rem : un COPDEM sur Rincon
rem Reprojection du COPDEM : gdal_warp sous QGIS > Copernicus_DSM_10_N18_00_W066_00_DEM_reproj32619.tif
rem Renommer le COPDEM : Copernicus_2011_DEM_reproj32619.tif

rem Découpage du DEM avec l'emprise de l'image Sentinel 2 découpée Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif
set input_COPDEM="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\DEM_Porto_Rico\Rincon\Copernicus_2011_DEM_reproj32619.tif"
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif"
set output_COPDEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\DEM_Porto_Rico\Rincon\Copernicus_2011_DEM_reproj32619_Rincon.tif"
call otbcli_extractroi -in %input_COPDEM% -mode fit -mode.fit.im %input_5B_clip% -out %output_COPDEM_clip% float

rem Rééchantillonnage du DEM par rapport à la résolution de l'image Sentinel 2 découpée Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\raster\Mosaic_S2B_MSIL2A_202002_B4_B3_B2_B8_B11_Rincon.tif"
set input_COPDEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\DEM_Porto_Rico\Rincon\Copernicus_2011_DEM_reproj32619_Rincon.tif"
set output_COPDEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\DEM_Porto_Rico\Rincon\Copernicus_2011_DEM_reproj32619_Rincon_10m.tif"
call otbcli_Superimpose -inr %input_5B_clip% -inm %input_COPDEM_clip% -out %output_COPDEM_10%

rem Binarisation mangroves = 1 et autres classes = 0 + filtrage avec l'altitude en prenant le COPDEM (==3 signifie la 3ème classe de la classification = mangroves)
rem Changer l'atltitude dans l'expression de BandMathX
set input_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\classification\Classif_RF_Rincon5B_nocloud.tif"
set input_COPDEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\DEM_Porto_Rico\Rincon\Copernicus_2011_DEM_reproj32619_Rincon_10m.tif"
set output_mangroves_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\binarisation-filtrage\Classif_RF_Rincon_nocloud_Mangroves_18m.tif"
call otbcli_BandMathX -il %input_RF_5B_clip_nocloud% %input_COPDEM_10% -out %output_mangroves_clip_nocloud% uint8 -exp "im2b1<=18&&im1b1==3?1:0"

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Porto_Rico\Rincon\lissage\

rem manip à faire manuellement sur QGIS : QGIS > Raster > Analyse > Tamiser (seuil = 5 ; Cocher "Ne pas utiliser le masque de validité par défaut pour les bandes en entrée") > nom de sortie : Classif_RF_Rincon_nocloud_Mangroves_18m_Tam5