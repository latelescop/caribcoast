rem objectif : détection de la mangrove sur le site Clugny en Guadeloupe
rem bandes utilisées : B4 (rouge), B3 (vert), B2 (bleu), B8 (PIR) et B11 (SWIR) > sortir les 5 bandes du dossier Sentinel 2 téléchargé (chemin d'accès trop long)
rem en amont : 
	rem 1. création de l'emprise de la zone de l'étude, 
	rem 2. création d'un masque de nuages + ombres,
	rem 3. création des bases de de données apprentissage et validation,
	rem 4. traitements du DEM.
rem une seule image couvrant la zone
rem date : 27/01/2020
rem projection : EPSG 32620

set PATH="C:\OTB-7.2.0-Win64\bin";%PATH%

rem Rééchantillonnage de la bande 11 à 20m avec la bande 4 à 10m > bande 11 à 10m
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B04_10m.jp2"
set input_B11_20="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B11_20m.jp2"
set output_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B11_10m.jp2"
call otbcli_Superimpose -inr %input_B4_10% -inm %input_B11_20% -out %output_B11_10% uint16

rem Fusion des bandes B4, B3, B2, B8 et B11, toutes à 10m
set input_B4_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B04_10m.jp2"
set input_B3_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B03_10m.jp2"
set input_B2_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B02_10m.jp2"
set input_B8_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B08_10m.jp2"
set input_B11_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\T20QPD_20200127T144701_B11_10m.jp2"
set output_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11.tif"
call otbcli_ConcatenateImages -il %input_B4_10% %input_B3_10% %input_B2_10% %input_B8_10% %input_B11_10% -out %output_5B% uint16

rem Découpage de l'image selon l'emprise de la zone de l'étude
set input_5B="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11.tif"
set input_emprise="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\vecteur\zone_Clugny.shp"
set output_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif"
call otbcli_extractroi -in %input_5B% -mode fit -mode.fit.vect %input_emprise% -out %output_5B_clip% uint16

rem MASQUE NUAGES + OMBRES

rem Rasterisation du masque
set input_mask_shp="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\masque\mask_nuages_S2_20200127.shp"
set input_ref_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif"
set output_mask_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\masque\mask_nuages_S2_20200127.tif"
call otbcli_rasterization -in %input_mask_shp% -im %input_ref_5B_clip% -szx 10 -szy 10 -epsg 32620 -background 0 -mode attribute -mode.attribute.field id -out %output_mask_raster%

rem Application du masque de nuages + ombres sur S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif"
set input_mask_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\masque\mask_nuages_S2_20200127.tif"
set output_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny_nocloud.tif"
call otbcli_BandMathX -il %input_5B_clip% %input_mask_raster% -out %output_5B_clip_nocloud%?nodata=65535 uint16 -exp "im2b1==1?{65535,65535,65535,65535,65535}:im1"

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\

rem CLASSIFICATION RANDOM FOREST

rem Inversion du masque nuages + ombres
set input_mask_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\masque\mask_nuages_S2_20200127.tif"
set output_mask_inv_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\masque\mask_nuages_S2_20200127_inv.tif"
call otbcli_BandMathX -il %input_mask_raster% -out %output_mask_inv_raster% uint8 -exp "im1b1==1?0:1"

rem Calcul des statistiques de l'image raster S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny
set input_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny_nocloud.tif"
set output_stat_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\stat_S2A_MSIL2A_Clugny5B_nocloud.xml"
call otbcli_ComputeImagesStatistics -il %input_5B_clip_nocloud% -out %output_stat_5B_clip_nocloud%

rem Entraînement du modèle statistique avec les données d'apprentissage
set input_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny_nocloud.tif"
set input_BD_app="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\vecteur\BD\BD_Clugny_Apprentissage.shp"
set input_stat_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\stat_S2A_MSIL2A_Clugny5B_nocloud.xml"
set output_model_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\RF_S2A_MSIL2A_Clugny5B_nocloud.model"
set output_confmatout_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\RF_S2A_MSIL2A_Clugny5B_nocloud.csv"
call otbcli_TrainImagesClassifier -io.il %input_5B_clip_nocloud% -io.vd %input_BD_app% -io.imstat %input_stat_5B_clip_nocloud% -sample.vtr 0.5 -sample.vfn id -classifier rf -classifier.rf.min 20 -classifier.rf.nbtrees 200 -io.out %output_model_5B_clip_nocloud% -io.confmatout %output_confmatout_5B_clip_nocloud%

rem Classification de l'image avec le modèle statistique
set input_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny_nocloud.tif"
set input_mask_inv_raster="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\masque\mask_nuages_S2_20200127_inv.tif"
set input_model_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\RF_S2A_MSIL2A_Clugny5B_nocloud.model"
set input_stat_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\stat_S2A_MSIL2A_Clugny5B_nocloud.xml"
set output_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\Classif_RF_Clugny5B_nocloud.tif"
call otbcli_ImageClassifier -in %input_5B_clip_nocloud% -mask %input_mask_inv_raster% -model %input_model_5B_clip_nocloud% -imstat %input_stat_5B_clip_nocloud% -out %output_RF_5B_clip_nocloud% uint8

rem Evaluation statistique de la classification avec la matrice de confusion
set input_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\Classif_RF_Clugny5B_nocloud.tif"
set output_matix_conf_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\Matrix_Conf_classif_RF_Clugny5B_nocloud.csv"
set input_BD_val="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\vecteur\BD\BD_Clugny_Validation.shp"
call otbcli_ComputeConfusionMatrix -in %input_RF_5B_clip_nocloud% -out %output_matix_conf_RF_5B_clip_nocloud% -ref vector -ref.vector.in %input_BD_val% -ref.vector.field id
rem copie d'écran à faire pour avoir les informations de la précision, du rappel et du f-score de chaque classe

rem pas de classe MANGROVES donc pas de binarisation-filtrage.

rem call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\binarisation-filtrage\

rem Conversion du DEM vrt en DEM tif avec gdalwarp 
rem gdalwarp DEM.vrt DEM.tif -ot float32

rem attention à la projection du DEM

rem Découpage du DEM avec l'emprise de l'image Sentinel 2 découpée S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif
set input_DEM="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\DEM_Guadeloupe\litto3d\LITTO3D_GUA_5M_GUAD88.tif"
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif"
set output_DEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\DEM_Guadeloupe\litto3d\LITTO3D_GUA_5M_GUAD88_Clugny.tif"
call otbcli_extractroi -in %input_DEM% -mode fit -mode.fit.im %input_5B_clip% -out %output_DEM_clip%

rem Rééchantillonnage du DEM par rapport à la résolution de l'image Sentinel 2 découpée S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif
set input_5B_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\raster\S2A_MSIL2A_20200127_B4_B3_B2_B8_B11_Clugny.tif"
set input_DEM_clip="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\DEM_Guadeloupe\litto3d\LITTO3D_GUA_5M_GUAD88_Clugny.tif"
set output_DEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\DEM_Guadeloupe\litto3d\LITTO3D_GUA_10M_GUAD88_Clugny.tif"
call otbcli_Superimpose -inr %input_5B_clip% -inm %input_DEM_clip% -out %output_DEM_10%

rem Binarisation mangroves = 1 et autres classes = 0 + filtrage avec l'altitude en prenant un DEM (==3 signifie la 3ème classe de la classification = mangroves)
rem Changer dans l'expression de bandMathX l'altitude
rem set input_RF_5B_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\classification\Classif_RF_Clugny5B_nocloud.tif"
rem set input_DEM_10="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\DEM_Guadeloupe\litto3d\LITTO3D_GUA_10M_GUAD88_Clugny.tif"
rem set output_mangroves_clip_nocloud="C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\binarisation-filtrage\Classif_RF_Clugny_nocloud_Mangroves_1_5m.tif"
rem call otbcli_BandMathX -il %input_RF_5B_clip_nocloud% %input_DEM_10% -out %output_mangroves_clip_nocloud% uint8 -exp "im2b1<=1.5&&im1b1==3?1:0" 

call mkdir C:\Users\w10\Documents\PROJETS\PRESTATIONS\CaribCoast\Guadeloupe\Clugny\lissage\

rem manip à faire manuellement sur QGIS : QGIS > Raster > Analyse > Tamiser (seuil = 5 ; Cocher "Ne pas utiliser le masque de validité par défaut pour les bandes en entrée") > nom de sortie : Classif_RF_Clugny_nocloud_1-5m_Tam5